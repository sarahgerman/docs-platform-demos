/**
 * Couple of tweaks for nicer Docs demos
 * Not intended for production!
 */

/**
 * Hides badge markup
 */
const removeBadgeMarkup = () => {
  const badgeElements = document.querySelectorAll(
    "h1, h2, h3, h4, h5, h6, .td-sidebar-nav__section span, .breadcrumb-item, #TableOfContents a"
  );

  const regex = /\([A-Z\s]+\)/g; // words in all-caps within parentheses

  badgeElements.forEach(function (el) {
    const elText = el.textContent;

    let cleanText = elText.replace(regex, "");
    cleanText = cleanText.replaceAll("*", "");
    cleanText = cleanText.replace(/\s+/g, " ");
    cleanText = cleanText.trim();

    el.textContent = cleanText;
  });
};

window.onload = () => {
  removeBadgeMarkup();
};
