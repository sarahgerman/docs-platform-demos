module gitlab.com/sarahgerman/docs-platform-demos

go 1.21.0

require (
	github.com/google/docsy v0.7.1 // indirect
	github.com/google/docsy/dependencies v0.7.1 // indirect
)
