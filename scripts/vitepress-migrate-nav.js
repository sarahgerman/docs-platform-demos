#!/usr/bin/env node

const fs = require("fs");
const yaml = require("js-yaml");

// Input and output files
const inputYamlFile = "data/navigation/navigation.yaml";
const outputYamlFile = "content/navigation.yaml";
const outputJsonFile = "content/navigation.json";

// Define the find-and-replace mapping for YAML keys
const findReplaceMapping = {
  sections: "sidebar",
  section_title: "text",
  section_url: "link",
  section_categories: "items",
  category_title: "text",
  category_url: "link",
  docs: "items",
  doc_title: "text",
  doc_url: "link",
};

// Load the YAML data from the input file
const yamlData = yaml.load(fs.readFileSync(inputYamlFile, "utf8"));

// Helper function to perform find-and-replace recursively
function findAndReplaceKeys(obj, mapping) {
  if (Array.isArray(obj)) {
    return obj.map((item) => findAndReplaceKeys(item, mapping));
  } else if (typeof obj === "object") {
    const newObj = {};
    for (const key in obj) {
      if (key in mapping) {
        newObj[mapping[key]] = findAndReplaceKeys(obj[key], mapping);
      } else {
        newObj[key] = findAndReplaceKeys(obj[key], mapping);
      }
    }
    return newObj;
  } else {
    return obj;
  }
}

// Replace keys in the YAML file
const modifiedYamlData = findAndReplaceKeys(yamlData, findReplaceMapping);

/**
 * Updates link prefixes
 *   - ee/ => gitlab/
 *   - All products: add leading /
 */
function updateLinkPrefixes(obj) {
  if (typeof obj === "object") {
    if (Array.isArray(obj)) {
      for (let i = 0; i < obj.length; i++) {
        obj[i] = updateLinkPrefixes(obj[i]);
      }
    } else {
      for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
          obj[key] = updateLinkPrefixes(obj[key]);
          if (key === "link" && typeof obj[key] === "string") {
            obj[key] = obj[key].replace(/ee\//g, "gitlab/");
            obj[key] = `/${obj[key]}`;
          }
        }
      }
    }
  }
  return obj;
}

const happyNav = updateLinkPrefixes(modifiedYamlData);

// Write the new YAML file
fs.writeFileSync(outputYamlFile, yaml.dump(happyNav));

// Build a JSON version as well
// Eventually we'd want the app to read the YAML file, but this is fine for the demo!
// https://vitepress.dev/guide/data-loading
const jsonData = JSON.stringify(happyNav, null, 2);
fs.writeFileSync(outputJsonFile, jsonData);
