#!/bin/bash

# Rename index files
find ./content -type f -name 'index.md' -execdir mv '{}' '_index.md' \;

# Fix front matter
find ./content -type f -name "*.md" | parallel ./scripts/hugo-update-frontmatter.rb

# Copy demo files to the target directory
cp ./hugo/assets/demo-pages/* ./content
