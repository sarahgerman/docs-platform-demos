#!/bin/bash

# Just for demo purposes:
# 1. Drop contributor docs
rm -rf ./content/gitlab/development
rm -rf ./content/gitlab/architecture/blueprints
rm -rf ./content/omnibus/development
rm -rf ./content/runner/development
rm -rf ./content/charts/development
# 2. Drop pages that throw errors on build
rm -rf ./content/gitlab/api/settings.md
rm -rf ./content/gitlab/update/deprecations.md
rm -rf ./content/gitlab/user/project/changelogs.md
rm -rf ./content/gitlab/user/project/pages/index.md
rm -rf ./content/runner/configuration/advanced-configuration.md

# Update navigation
node ./scripts/vitepress-migrate-nav.js

# Fix front matter
find ./content -type f -name "*.md" | parallel ./scripts/vitepress-remove-redirects.rb

# Copy demo files to the target directory
cp -r ./vitepress/public ./content
cp -r ./vitepress/demo-pages/* ./content
