#!/usr/bin/env ruby

require 'yaml'

# Read the input markdown file
input_file = ARGV[0]

# Read the content of the markdown file
markdown_content = File.read(input_file)

# Extract YAML front matter and body
front_matter, body = markdown_content.split(/^---\n/m, 3)[1, 2].map(&:strip)

if front_matter
    front_matter_data = YAML.safe_load(front_matter)

    unless front_matter_data['redirect_to']
        # Delete type data, this is unused and conflicts with Hugo
        front_matter_data.delete('type')

        # Extract the h1 header and remove it from the body,
        # then set it as the title in the front matter.
        title = body.match(/^# (.+)$/)&.captures&.first
        body.sub!(/^# (.+)$/, '')
        front_matter_data['title'] = title
    end

    # Hide redirects and pages without titles from the navigation and Lunr search
    if front_matter_data['redirect_to'] || !front_matter_data['title']
        front_matter_data['toc_hide'] = true
        front_matter_data['exclude_search'] = true
    end

    updated_markdown = "#{front_matter_data.to_yaml.strip}\n---\n\n#{body.strip}"

    # Write the updated content back to the file
    File.write(input_file, updated_markdown)

else
    puts "No valid YAML front matter found in #{input_file}. Removing from build..."
    File.delete(input_file)
end
