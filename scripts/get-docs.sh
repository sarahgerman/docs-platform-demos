#!/bin/bash

# Set up the content directory
if [ -d "content" ]; then
    echo "Removing existing content directory"
    rm -rf "content"
fi
mkdir content

# Create a temporary directory to clone repositories
temp_dir=$(mktemp -d)

# Clean up the temporary directory on script exit
cleanup() {
    rm -rf "content/$temp_dir"
    echo "Temporary directory removed."
}

trap cleanup EXIT

# Parse the YAML file and extract repository information
repos_yaml="products.yaml"

# Iterate over each repository entry in the YAML file
for entry in $(yq eval '.products | keys | .[]' "$repos_yaml"); do
    repo_url=$(yq eval ".products[\"$entry\"].repo" "$repos_yaml")
    content_dir=$(yq eval ".products[\"$entry\"].content_dir" "$repos_yaml")

    # Clone the repository
    git clone --depth 1 "$repo_url" "$temp_dir/$entry"
    
    # Move the content directory to the appropriate location
    mv "$temp_dir/$entry/$content_dir" "content/$entry"
done

echo "Cloning and moving completed."
