#!/usr/bin/env ruby

require 'yaml'

# Read the input markdown file
input_file = ARGV[0]

# Read the content of the markdown file
markdown_content = File.read(input_file)

# Extract YAML front matter and body
front_matter, body = markdown_content.split(/^---\n/m, 3)[1, 2].map(&:strip)

if front_matter
    front_matter_data = YAML.safe_load(front_matter)

    # Delete redirects for now to avoid build errors
    if front_matter_data['redirect_to']
        File.delete(input_file)
    end
else
    # Delete files without front matter
    puts "No valid YAML front matter found in #{input_file}. Removing from build..."
    File.delete(input_file)
end
