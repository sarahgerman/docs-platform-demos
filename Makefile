INFO = \033[32m
END = \033[0m

setup:
	@printf "\n$(INFO)INFO: Updating asdf plugins...$(END)\n"
	@asdf plugin add ruby || true
	@asdf plugin add nodejs || true
	@asdf plugin add yarn || true
	@asdf plugin add golang || true
	@asdf plugin update ruby
	@asdf plugin update nodejs
	@asdf plugin update yarn
	@asdf plugin update golang
	@printf "\n$(INFO)INFO: Installing dependencies...$(END)\n"
	@asdf install
	brew bundle --no-lock

update:
	@echo "\n$(INFO)INFO: Cloning GitLab projects...$(END)\n"
	@./scripts/get-docs.sh

run-nuxt:
	@cd ./nuxt && yarn i && yarn run dev

run-hugo:
	@printf "\n$(INFO)INFO: Updating content for Hugo. This may take a few minutes...$(END)\n"
	@./scripts/hugo-migrate.sh
	@printf "\n$(INFO)INFO: Installing JavaScript dependencies...$(END)\n"
	@npm i --prefix ./hugo
	@printf "\n$(INFO)INFO: Starting Hugo...$(END)\n"
	@cd ./hugo && hugo mod get -u && hugo server

run-vitepress:
	@printf "\n$(INFO)INFO: Updating content for Vitepress...$(END)\n"
	@npm i
	@./scripts/vitepress-migrate.sh
	@printf "\n$(INFO)INFO: Installing JavaScript dependencies...$(END)\n"
	@npm i --prefix ./vitepress
	@printf "\n$(INFO)INFO: Starting Vitepress...$(END)\n"
	@cd ./vitepress && npm run docs:dev
