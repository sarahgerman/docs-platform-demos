---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "GitLab Docs"
  text: "A VitePress Site"
  tagline: My great project tagline
  actions:
    - theme: brand
      text: Tutorials
      link: /gitlab/tutorials/
    - theme: alt
      text: Install GitLab
      link: /gitlab/install/

features:
  - title: Feature A
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature B
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature C
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---
