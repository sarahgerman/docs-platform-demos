import { defineConfig } from "vitepress";
import nav from "../../content/navigation.json";

// https://vitepress.dev/reference/site-config
export default defineConfig({
  ignoreDeadLinks: true,
  markdown: { attrs: { disable: true } },
  title: "GitLab Docs",
  description: "GitLab product documentation.",
  srcDir: "../content",
  themeConfig: {
    logo: "/gitlab-logo-500.svg",
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      {
        text: "What's new?",
        link: "https://about.gitlab.com/releases/categories/releases/",
      },
      {
        text: "Get free trial",
        link: "https://gitlab.com/-/trials/new?glm_content=navigation-cta-docs&glm_source=docs.gitlab.com",
      },
    ],
    sidebar: nav.sidebar,
    socialLinks: [
      { icon: "gitab", link: "https://gitlab.com/gitlab-org/gitlab-docs" },
    ],
  },
});
