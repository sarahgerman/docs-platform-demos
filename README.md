# Docs Platform Demos

Demos for GitLab Docs on:

- Hugo, with Docsy theme
- Vitepress
- ~~Nuxt, with Docus theme~~

Known issues:

- Vitepress can run in dev/preview mode but cannot run a full build (memory limit issues likely due to site size).
- Nuxt will not start up at all (memory limit issues likely due to site size).

## Setup

Clone the project and install system dependencies:

```shell
git clone git@gitlab.com:sarahgerman/docs-platform-demos.git
cd docs-platform-demos
make setup
```

## Run the demos

Run these commands from the project root directory (e.g, `~/dev/docs-platform-demos`).

1. Pull in fresh content: `make update`
1. Run a demo by running one of these:

    ```shell
    make run-hugo # hugo
    make run-vitepress # vitepress
    ```

    This will start the local development environment for the chosen framework.

1. View the demo site at the localhost URL returned by the previous command.

If you want to run a different demo, run `make update` again. The demos make
different changes to the source markdown files.

## References

- [Create a new Hugo site from scratch with Docsy as a Hugo Module](https://www.docsy.dev/docs/get-started/docsy-as-module/start-from-scratch/)
